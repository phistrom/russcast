#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Created by Phillip Stromberg
Created on 2014-05-29
"""

from russcast.podcast_server.flask_app import app

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=32000)