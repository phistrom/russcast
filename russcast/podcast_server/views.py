# !/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Created by Phillip Stromberg
Created on 5/29/2014
"""

from datetime import date, datetime, timedelta
import flask
from flask import abort, make_response, redirect, render_template, request
from russcast.podcast_server.flask_app import requires_auth
from russcast.database import Episode
from russcast.podcast_server.flask_app import app


@app.route('/')
def index():
    return render_template('home.jinja2')


@app.route('/podcast.xml', defaults={'limit': 30})
@requires_auth
def podcast_xml(limit=30):
    episodes = Episode.query.order_by(Episode.date.desc()).filter(Episode.date >= date.today() - timedelta(days=limit))
    resp = make_response(render_template('podcast_xml.jinja2', episodes=episodes))
    resp.headers['Content-Type'] = 'application/xml'
    return resp


@app.route('/test-login')
@requires_auth
def nginx_return_auth():
    """Designed to be used with nginx. If one WANTED to host their own copies of the MP3s, nginx can ask
    Flask if a login is valid. If it is, nginx can then stream the MP3 to the user. It is much more preferable
    than nginx streams the MP3 to the user than Flask."""
    return ""