# -*- coding: utf-8 -*-
"""
Created by Phillip Stromberg
Created on 2014-05-30
"""

import json
import redis
import os


def get_redis():
    try:
        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'redis.json'), 'rb') as rfile:
            redis_options = json.load(rfile)
            return redis.StrictRedis(**redis_options)
    except:
        return redis.StrictRedis()  # return with defaults