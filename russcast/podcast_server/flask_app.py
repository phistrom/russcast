# -*- coding: utf-8 -*-
"""
The setup for the Flask app that serves the podcast.xml and authenticates incoming users.

Created by Phillip Stromberg
Created on 2014-05-29
"""


from flask import abort, Flask, request, Response
from flask.ext.sqlalchemy import SQLAlchemy
from functools import wraps
import logging
import os
from russcast.podcast_server import get_redis
from russcast import database_setup
import traceback


app = Flask(__name__)
app.secret_key = open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'secret_key.txt'), 'rb').read()
_json = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'db.json')
app.config['SQLALCHEMY_DATABASE_URI'] = database_setup.get_connection_string_from_file(_json)

database_setup.db = SQLAlchemy(app)
redis_db = get_redis()

import russcast.database
from russcast.website import WebsiteConnection


@app.before_first_request
def setup_logging():
    if not app.debug:
        app.logger.addHandler(logging.StreamHandler())
        app.logger.setLevel(logging.INFO)


def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'You need a russmartin.fm login to use this site.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="A russmartin.fm Login Required"'})


def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    login = "%s:%s" % (username, password)
    try:
        cached_status = int(redis_db.get(login))
        return True if cached_status == 1 else False
    except TypeError:
        pass  # None type means login is not cached
    try:
        if not (username and password):
            raise Exception()
        conn = WebsiteConnection(username=username, password=password)
    except Exception:
        redis_db.set(login, '0', 60)
        return False
    redis_db.set(login, '1', 300)
    return True


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        try:
            if not auth or not check_auth(auth.username, auth.password):
                return authenticate()
        except Exception as ex:
            logging.error(traceback.format_exc())
            abort(503)
        return f(*args, **kwargs)
    return decorated


import russcast.podcast_server.views