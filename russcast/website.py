# !/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Created by Phillip Stromberg
Created on 5/29/2014
"""

import bs4
from datetime import date, datetime, timedelta
import json
from mutagen.mp3 import MP3
import os
import re
import requests
from russcast.exc import *
import urlparse
import tempfile
import traceback

LOGIN_URL = 'http://www.russmartin.fm/wp-login.php'
DOWNLOADS_URLS = (
    'http://www.russmartin.fm/download-audio-1/',
    'http://www.russmartin.fm/download-audio/',
    'http://www.russmartin.fm/download-show-audio/',
)
DEFAULT_LOGIN_FILE = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'rmsfm.json')
DEFAULT_USER_AGENT = 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)'

SHOW_DOWNLOAD_URL_REGEX = re.compile(r'<p>\s*(?P<date>[\d\-]+)\s+&gt;.*Download\s*</a>\s*</p>\s*$', re.IGNORECASE)
MP3_REGEX = re.compile(r'(?P<month>\d+)\-(?P<day>\d+)\-(?P<year>\d{2}).*\.mp3')
VERBOSE = True


class WebsiteConnection(object):
    def __init__(self, login_file=None, username=None, password=None):
        global Episode
        from russcast.database import Episode
        if not (login_file or (username and password)):
            login_file = DEFAULT_LOGIN_FILE
        if login_file:
            with open(login_file, 'rb') as infile:
                login = json.load(infile)
                username = login.get('log')
                password = login.get('pwd')
        if username and password:
            self.username = username
            self.password = password
        else:
            raise ValueError('Cannot connect to %s without a login/password or a login JSON file.'
                             % LOGIN_URL)

        self.session = requests.Session()
        self.session.headers.update({
            'User-Agent': DEFAULT_USER_AGENT
        })

        self._podcasts = []

        self.login()

    @property
    def podcasts(self):
        if not self._podcasts:
            self.refresh_podcasts()
        return self._podcasts

    @classmethod
    def download_episode(cls, episode, outfile):
        if VERBOSE:
            print "Downloading %s" % episode
        resp = requests.get(episode.uri, stream=True, headers={'User-Agent': DEFAULT_USER_AGENT})
        for block in resp.iter_content(chunk_size=1024):
            if block:
                outfile.write(block)
        resp.close()
        if VERBOSE:
            print "Finished downloading %s" % episode

    @classmethod
    def get_episode_metadata(cls, episode, filepath=None):
        if filepath:
            episode.size = os.stat(filepath).st_size
            temporary = False
        else:
            with tempfile.NamedTemporaryFile(delete=False, mode='wb') as tempf:
                cls.download_episode(episode, tempf)
                tempf.seek(0, os.SEEK_END)  # move read pointer to very end of file
                episode.size = tempf.tell()  # index of last position of file = size in bytes
                filepath = tempf.name
                temporary = True
        mp3 = MP3(filepath)
        seconds = int(mp3.info.length)
        episode.duration = timedelta(seconds=seconds)
        if temporary:
            os.remove(filepath)


    def login(self):
        form_data = {
            'log': self.username,
            'pwd': self.password,
            'wp-submit': 'Login',
            'wlm_redirect_to': 'wishlistmember'
        }
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Referer': 'http://www.russmartin.fm/become-a-member/'
        }

        print "Logging in to %s" % LOGIN_URL
        # first try logging in
        response = self.session.post(LOGIN_URL, data=form_data, headers=headers, allow_redirects=True)
        if response.status_code not in (200, 301, 302):
            raise ConnectionError('Received status code %s when logging in to %s' % (response.status_code, LOGIN_URL))
        if response.url == LOGIN_URL:
            raise BadPasswordError('Incorrect username or password for %s' % LOGIN_URL)
        # now try and access a download page, if login failed, we should get a 302 instead of 200
        response = self.session.get(DOWNLOADS_URLS[0], allow_redirects=False)
        if response.status_code != 200:
            raise NotLoggedIn('Has your password has expired?')

    def refresh_podcasts(self, urls=None):
        if not urls:
            urls = DOWNLOADS_URLS
        episodes = []
        for page_url in urls:
            try:
                html = self._fetch_page(page_url)
                episodes.extend(self._parse_page(html))
            except NotLoggedIn as ex:
                print traceback.format_exc()
                print ex.message
        self._podcasts[:] = episodes

    @classmethod
    def parse_date(cls, p):
        """Try to parse the show's air date out of the haphazard data entry
        they use on the website."""
        match = SHOW_DOWNLOAD_URL_REGEX.match(str(p))
        if not match:
            return None
        sdate = match.group('date')
        url = p.contents[1].get('href')
        try:
            month, day, year = sdate.split('-')
            air_date = date(int(year) + 2000, int(month), int(day))
        except (ValueError,):
            air_date = cls._parse_date_from_url(url)

        return air_date

    @classmethod
    def _parse_date_from_url(cls, url):
        base = os.path.basename(url)
        match = MP3_REGEX.match(base)
        month = int(match.group('month'))
        day = int(month.group('day'))
        year = int(month.group('year'))
        if year <= 90:
            year += 2000
        else:
            year += 1900
        return date(year, month, day)

    def _fetch_page(self, url):
        resp = self.session.get(url, allow_redirects=False)
        if resp.status_code != 200:
            raise NotLoggedIn("Tried to access %s but got HTTP Status %s" % (url, resp.status_code))
        page = resp.text.encode('utf-8', 'ignore')
        return page

    def _parse_page(self, html):
        soup = bs4.BeautifulSoup(html)
        ptags = []
        episodes = []
        for p in soup.find_all('p'):
            try:
                air_date = self.parse_date(p)
                if not air_date:
                    continue
                uri = p.contents[1].get('href')
            except Exception as ex:
                if VERBOSE:
                    print ex.message
                continue

            episode = Episode(date=air_date, uri=uri)
            episodes.append(episode)

        return episodes