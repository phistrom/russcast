# !/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Created by Phillip Stromberg
Created on 5/29/2014
"""


class BadPasswordError(Exception):
    pass


class ConnectionError(Exception):
    pass


class NotLoggedIn(Exception):
    pass