# !/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Created by Phillip Stromberg
Created on 5/29/2014
"""

from datetime import datetime
import os
from sqlalchemy import Table, Column, String, ForeignKey, Text, BOOLEAN, UniqueConstraint, DateTime, ForeignKeyConstraint, DATE
from sqlalchemy.sql import and_, exists
from sqlalchemy.dialects.mysql import INTEGER as Integer, TIME, TEXT
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm import deferred, relationship, validates, Session
from sqlalchemy.ext.hybrid import hybrid_property
from website import WebsiteConnection

try:
    from database_setup import db
    Base = db.Model
    print "Got a db.Model"
except (AttributeError, ImportError):
    from sqlalchemy.ext.declarative import declarative_base
    Base = declarative_base()
    print "Got declarative base"


class Episode(Base):
    __tablename__ = 'episodes'
    date = Column('Date', DATE, primary_key=True)
    uri = Column('URI', String(255), nullable=False)
    size = Column('Size', Integer(unsigned=True), nullable=True)
    duration = Column('Duration', TIME, nullable=True)
    summary = Column('Summary', TEXT, nullable=True)

    def download(self, download_dir):
        filename = "%s.mp3" % self.date
        filepath = os.path.join(download_dir, filename)
        if os.path.exists(filepath):
            print "Episode %s already downloaded" % self.date
            return
        with open(filepath, 'wb') as outfile:
            WebsiteConnection.download_episode(self, outfile)

    def __hash__(self):
        hash(self.date)

    def __eq__(self, other):
        try:
            return type(self) == type(other) and self.date == other.date
        except:
            return False

    def __str__(self):
        return "%s: %s - (%s, %s)" % (self.date, self.uri, self.size, self.duration)


class LogEntry(Base):
    __tablename__ = "log"
    id = Column('ID', Integer(unsigned=True), primary_key=True, autoincrement=True)
    access_date = Column('Access_Date', DateTime, nullable=False, default=datetime.now)
    username = Column('Username', String(100), nullable=False)
    event = Column('Event', String(100), nullable=False)
    remote_ip = Column('Remote_IP', String(45), nullable=False)
    user_agent = Column('User_Agent', String(500), nullable=False)