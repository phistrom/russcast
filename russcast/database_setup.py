# -*- coding: utf-8 -*-
"""

Created by Phillip Stromberg
Created on 5/29/2014
"""

import json

PYMYSQL_STRING_TEMPLATE = "mysql+pymysql://%s:%s@%s/%s?charset=utf8"
db = None  # to be replaced by an initializing module


def get_connection_string_from_file(json_path):
    """Returns a connection string that SQLAlchemy can use given a JSON
    file that contains user, passwd, host, and db fields."""
    with open(json_path, 'rb') as json_file:
        conn_params = json.load(json_file)
    # check to see if all parameters we need are in this JSON dictionary
    if {'user', 'passwd', 'host', 'db'} <= set(conn_params):
        return PYMYSQL_STRING_TEMPLATE % (conn_params['user'], conn_params['passwd'],
                                          conn_params['host'], conn_params['db'])
    else:
        raise ValueError("The given JSON file at %s did not contain "
                         "all the parameters needed for a connection." % json_path)