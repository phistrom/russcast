# !/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Script for connecting to russmartin.fm and getting the latest list of downloadable
episodes. Checks for deletions and additions for the xml file.

Created by Phillip Stromberg
Created on 5/29/2014
"""

import os
from russcast.database import Base, Episode
from russcast.database_setup import get_connection_string_from_file
from russcast.website import WebsiteConnection
from sqlalchemy import create_engine, or_
from sqlalchemy.orm import sessionmaker


def main():
    json_loc = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'db.json')
    engine = create_engine(get_connection_string_from_file(json_loc))
    Base.metadata.create_all(engine)
    Session = sessionmaker(engine)
    session = Session()

    conn = WebsiteConnection()

    for ep in conn.podcasts:
        session.merge(ep)

    new_episodes = session.query(Episode)\
        .filter(or_(Episode.duration == None, Episode.size == None)).all()

    for ep in new_episodes:
        print "Updating info for %s" % ep
        conn.get_episode_metadata(ep)
        session.commit()

    if not new_episodes:
        print "Nothing to update."

if __name__ == '__main__':
    main()