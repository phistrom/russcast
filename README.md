# The Russ Martin Show Podcast #

### About ###
A podcast server for The Russ Martin Show at KEGL in Dallas, TX. If you have a login to the [www.russmartin.fm](http://www.russmartin.fm), it will scrape the downloads page for MP3 download links and add them to a database. It uses this database to create an XML file that podcast clients like iTunes and BeyondPod can use to download show MP3s. It requires the user to input their username and password for russmartin.fm and then checks it against their website. If the login is successful, access is given to the XML file. This way no one gets free episodes (and that is good, because subscriptions pay for Russ Martin's fallen police and firefighter foundation).

### Installation ###
1. Clone the repo
2. Make a virtual environment
3. Use the command `pip install -r requirements.txt` on the requirements.txt in the repository to install all the necessary libraries
4. [redis](http://redis.io/) is required for caching logins (so we don't bother the russmartin.fm site too much)
5. You can run the server by executing podcast_server.py or by installing gunicorn and executing something like `gunicorn -b unix:///var/run/podcast_server.sock podcast_server:app`
6. Before the server can run properly, you will need to generate db.json and rmsfm.json files and a secret_key.txt file. Examples of these will be included with the repository soon.

### License ###
Copyright 2014 Phillip Stromberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.