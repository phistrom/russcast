# !/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Created by Phillip Stromberg
Created on 5/30/2014
"""

import os
from russcast.database import Base, Episode
from russcast.database_setup import get_connection_string_from_file
from russcast.website import WebsiteConnection
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import sys


def main(download_dir):
    engine = create_engine(get_connection_string_from_file('db.json'))
    Base.metadata.create_all(engine)
    Session = sessionmaker(engine)
    session = Session()
    conn = WebsiteConnection()
    for ep in conn.podcasts:
        ep.download(download_dir)
    for ep in conn.podcasts:
        filename = "%s.mp3" % ep.date
        filepath = os.path.join(download_dir, filename)
        WebsiteConnection.get_episode_metadata(ep, filepath)
        print "read metadata for %s" % ep.date
        session.merge(ep)
    session.commit()


if __name__ == '__main__':
    download_dir = sys.argv[1]
    main(download_dir)